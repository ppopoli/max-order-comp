#include "Node.h"

Node :: Node(int a): number(a){ }

Node :: ~Node() {
  for(auto e : this->edges)
    free(e);
}

bool Node :: operator == (Node const & rhs)
{
  bool operator == (Node const&, Node const&);
  return this->number == rhs.number;
}

bool Node::operator !=(Node const & rhs){
  return !(this == &rhs); 
}

int Node :: getNumber()
{
  return this ->number;
}

int Node :: getSuffix()
{
  return this -> suffix;
}

void Node :: setSuffix(int i){
  this->suffix =i;
}

void Node :: addEdge(Edge* e){
  this->edges.push_back(e);
}

bool Node::hasPrimary() {
  Edge* e;
  for (auto it = edges.begin(); it != edges.end(); it++){
    e = *it;
    if (e->isPrimary())
      return true;
  }
  return false;
}

bool Node::hasPrimary(char a){
  Edge *e;
  for (vector<Edge*>::iterator it=edges.begin();it!= edges.end();++it){
    e = *it;
    if ((e->isPrimary())&&(e->getLabel()==a)){
      return true;
    }
  }
  return false;
}

Edge* Node::getPrimary(char a){
  Edge* e;
  for (vector<Edge*>::iterator it=edges.begin(); it!=edges.end();++it) {
    e = *it;
    if ((e->isPrimary())&&(e->getLabel()==a)) {
      return e;
    }
  }
  //should never come here!
  return NULL;
}

bool Node::hasSecondary(char a)
{
  for (vector<Edge*>::iterator it=edges.begin();it != edges.end();++it){
    Edge*e=*it;
    if(!(e->isPrimary()) && (e->getLabel()==a)){
      return true;
    }

  }
  return false;
}

Edge* Node::getSecondary(char a)
{
  for (vector<Edge*>::iterator it=edges.begin();it != edges.end();++it){
    Edge*e=*it;
    if(!(e->isPrimary()) && (e->getLabel()==a)){
      return e;
    }

  }
  //should never come here!
  return NULL;
}

bool Node::hasSecondaryAlterChild(int old, int neu)
{
  for(vector<Edge*>::iterator it =edges.begin();it !=edges.end();++it){
    Edge* e = *it;
    if(!(e->isPrimary())&&(e->getChild()==old)){
      (*it)->setChild(neu);
      return true;
    }
  }
  return false;

}

void Node::getSecondaryAlterChild(int old, int neu)
{
  for (vector<Edge*>::iterator it=edges.begin();it!=edges.end();++it){
    if (((*it)->getChild()==old)&&((*it)->isPrimary()==false)){
      (*it)->setChild(neu);
      (*it)->setPrimality(true);
    }
  }
}

void Node::print(){
  cout << "number:" << number << "suffix:" << suffix << "ofedges:" << edges.size();
  for(int i=0; i<edges.size();i++) {
    edges[i]->print();
  }
}

int Node::size(){
  return edges.size()*sizeof(Edge*)+sizeof(int)*3;
}
