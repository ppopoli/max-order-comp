#ifndef EDGE_H
#define EDGE_H

#include<iostream>

using namespace std ;
 
class Edge
{
  int child;
  char label;
  bool isprimary;

public :
  Edge(int, char, bool);

  ~Edge();

  int getChild();
  void setChild(int);
  char getLabel();
  bool isPrimary();
  void setPrimality(bool);

  void print();
  
};
#endif

 
