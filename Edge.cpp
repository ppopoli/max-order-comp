#include "Edge.h"

Edge::Edge(int c, char l, bool prim): child(c), label(l), isprimary(prim){ }

Edge::~Edge(){ 
  delete &child;

}

bool Edge::isPrimary(){
  return this->isprimary;
}

void Edge::setPrimality(bool b){
  isprimary = b;
}

int Edge::getChild()
{
  return child;
}

void Edge::setChild(int i){
  child=i;
}

char Edge::getLabel()
{
  return label;
}

void Edge:: print(){
  cout << "child:"<< child <<"label"<<label <<"isprim"<<isprimary<<endl;

}

