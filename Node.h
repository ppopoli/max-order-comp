#ifndef NODE_H
#define NODE_H

#include <list>
#include <vector>
#include <iostream>

#include "Edge.h"
using namespace std;

class Node {
  int number;
  int suffix = 0;

public:
  int depth = 0;
  vector<Edge*> edges;

  Node(int);

  ~Node();

  bool operator==(Node const&);
  bool operator!=(Node const&);

  int getNumber();

  int getSuffix();
  void setSuffix(int);

  void addEdge(Edge*);

  bool hasPrimary();
  bool hasPrimary(char);
  Edge* getPrimary(char);

  bool hasSecondary(char);
  Edge* getSecondary(char);
  bool hasSecondaryAlterChild(int, int);
  void getSecondaryAlterChild(int ,int);

  void print();
  
  int size();

  
};
#endif
