ARCH := corei7-avx 

UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Linux)
	CC := g++
	EXT := 
endif
ifeq ($(UNAME_S),Darwin)
	#CC := /usr/local/opt/llvm/bin/clang++
	CC := g++
	EXT := _OSX
endif

CFLAGS := -march=$(ARCH) -O3 -m64 -std=c++11

COMPILE_COMMAND := $(CC) $(CFLAGS)

OUTPUT_FILE := MaxOrderComp_$(USER)$(EXT)
all: 	.
	$(COMPILE_COMMAND) MaxOrderComp.cpp Edge.cpp Node.cpp -o $(OUTPUT_FILE) 

test:
	make all
	./$(OUTPUT_FILE) ThueMorse1000000terms.txt

clean:
	rm -rf *csv *log $(OUTPUT_FILE)
	

