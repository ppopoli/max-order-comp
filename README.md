This program compute the Maximum Order Complexity of a sequence by using the DAWG algorithm. 

The program consists of 5 files: MaxOrderComp.cpp, Node.h, Node.cpp, Edge.h and Edge.cpp.

Store the value of the desired sequence in .txt file. 

The program will give you the steps,the value of the maximum order complexity and the ratio of successive steps. 

