#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <fstream>

#include <sstream>

#include "Node.h"
#include "Edge.h"

#include <unistd.h>
using namespace std;

#define LOG_PATH "./"
#define OUT_PATH "./"

vector<Node*> nodes;
vector<int> maxOrderComplexity;

Node* source = new Node(0);
string input = "";

void build(string);
void build(string* w, std::string filename);

Node* update(Node*, char);
Node* split(Node*, Node*);

int currentOrder = 0;

int main(int argc, char*argv[]){
  
  if (argc == 1){
    cout << "Missing Argument"<< endl;
    cout << "Please Add Filename As An Argument"<<endl;
    return -1;
  }
  string filename = argv[1];
  cout << "Reading File "<< filename << endl;

  ifstream myfilein;
  myfilein.open(filename);

  if (myfilein.is_open())
    {
      getline(myfilein,input);
      myfilein.close();
    }

  cout << "Calculating Max Order Comp For " << filename << endl;

  nodes.reserve(2*input.size());
  build(&input, filename);

  cout << "Finished" << endl << endl;
  
  return 0;
}

void build(string *w, std::string filename) {
  filename.erase(filename.size()-3);

  std::string logfilename;
  std::string outfilename;
  std::stringstream ss;

  logfilename = LOG_PATH+filename+"log";
  outfilename = OUT_PATH+filename+"csv";
  
  cout << "Saving results in " << outfilename << endl;
  cout << "Saving logs in " << logfilename << endl;
  
  ofstream myfileout, mylogout;
  myfileout.open(outfilename);

  myfileout << "N;M(S,N)\n"; 
  myfileout.close();

  mylogout.open(logfilename);
  auto start = std::chrono::system_clock::now();
  std::time_t start_time = std::chrono::system_clock::to_time_t(start);
  ss.str("");
  ss << "Started at " << std::ctime(&start_time) << "\n";
  mylogout << ss.str() << std::setw(12) << "N\t" << std::setw(8) << "M(S,N)\t" << std::setprecision(20) << "ratio" << "\n";
  mylogout.close();
      
  std::cout << std::fixed << std::setprecision(20);
  
  int oldOrder = currentOrder;
  int oldPosition = 1;
  
  int i = 0;

  nodes.reserve(2*w->size());
  
  nodes.push_back(source);    
  Node* currentsink = source;
  for(char& letter : *w){
    currentsink = update(currentsink, letter);
    maxOrderComplexity.push_back(currentOrder);
    ++i;
    if (oldOrder != currentOrder){
      myfileout.open(outfilename, std::ofstream::out | std::ofstream::app);
      mylogout.open(logfilename, std::ofstream::out | std::ofstream::app);
      
      myfileout << i << ";" << currentOrder << "\n";
      
      ss.str("");
      ss << std::setw(12) << i << "\t" << std::setw(8) << currentOrder << "\t"  << std::fixed<<std::setprecision(30) << i/(oldPosition+0.0) << std::endl;

      std::cout << ss.str(); 
      mylogout << ss.str(); 
            
      oldOrder = currentOrder;
      oldPosition = i;
      myfileout.close();
      mylogout.close();
    }
  }
  auto end = std::chrono::system_clock::now();
  std::time_t end_time = std::chrono::system_clock::to_time_t(end);
  ss.str("");
  ss << "Ended at " << std::ctime(&end_time) << "\n";
  mylogout.open(logfilename, std::ofstream::out | std::ofstream::app);
  mylogout << ss.str() << "\n";
  mylogout.close();

}


Node* update(Node* currentsink, char a){

  Node* newsink = new Node(nodes.size());
  
  Node* currentnode = currentsink;
  Node* suffixnode ;
  Node* childnode;

  Edge* e = new Edge(newsink->getNumber(),a,true);

  nodes.push_back(newsink);

  currentsink->addEdge(e);

  newsink->depth = currentsink->depth + 1;
  currentnode = currentsink;

  suffixnode = NULL;

  while((currentnode != source) && (suffixnode == NULL)){
    currentnode = nodes[currentnode->getSuffix()];

    if(currentnode->hasPrimary(a)){
      e = currentnode->getPrimary(a);
      suffixnode = nodes[e->getChild()];
    }
    else{
      if (currentnode->hasSecondary(a)){
	e = (currentnode->getSecondary(a));
	childnode = nodes[e->getChild()];
	suffixnode = split(currentnode, childnode);
      }
      else{
	e = new Edge(newsink->getNumber(), a, false); 
	currentnode->addEdge(e);
	if((currentnode->edges.size()>1)&&(currentnode->depth+1>currentOrder))currentOrder=currentnode->depth+1;
      }
    }
  }  
  if (suffixnode == NULL){
    suffixnode = source;
  }
  newsink->setSuffix(suffixnode->getNumber());
  
  return newsink;
}

Node* split(Node* parentnode, Node* childnode){
  Node* newchildnode = new Node(nodes.size());
  Node* currentnode ;
  Edge* e;
  char label;
  int child;
    
  nodes.push_back(newchildnode);
  parentnode->getSecondaryAlterChild(childnode->getNumber(),newchildnode->getNumber());
  newchildnode->depth=parentnode->depth+1;

  for(auto it = childnode->edges.begin(); it!= childnode->edges.end(); ++it){
    child = (*it)->getChild();
    label = (*it)-> getLabel();
    e = new Edge(child, label, false); 
    newchildnode -> addEdge(e);
  }
  newchildnode->setSuffix(childnode->getSuffix());

  childnode->setSuffix(newchildnode->getNumber());

  currentnode = parentnode; 

  while (currentnode != source){
    currentnode = nodes[currentnode->getSuffix()];

    if (currentnode->hasSecondaryAlterChild(childnode->getNumber(),newchildnode->getNumber())){
      //already done
    }
    else {
      break;
    }
  }

  return newchildnode;
}
